package com.salman.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.LocalTime;

@RestController
public class FunRestController {

    @GetMapping("/")
    public String hello(){
        return "Hello Salman, Time is : " + LocalDateTime.now() + "!";
    }
}
