FROM java:8
COPY . /var/java
WORKDIR /var/java
CMD ["gradle", "run ."]